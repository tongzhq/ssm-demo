package com.tzq.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Program Name: mybatis-demo
 * <p>
 * Description:
 * <p>
 * Created by tongzhq on 2021/12/28 17:22
 *
 * @author tongzhq
 * @version 1.0
 */
@ComponentScan("com.tzq")
@Import({JdbcConfig.class, MyBatisConfig.class})
@EnableTransactionManagement
public class AppConfig {


}
