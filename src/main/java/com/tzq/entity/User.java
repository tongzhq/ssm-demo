package com.tzq.entity;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

/**
 * user
 * @author 
 */
@Data
@Builder
public class User implements Serializable {
    private Integer id;

    private String name;

    private Integer age;

    private static final long serialVersionUID = 1L;
}