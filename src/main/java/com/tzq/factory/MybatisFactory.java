package com.tzq.factory;

import com.tzq.mapper.UserMapper;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;



/**
 * Program Name: mybatis-demo
 * <p>
 * Description:
 * <p>
 * Created by tongzhq on 2021/12/28 14:20
 *
 * @author tongzhq
 * @version 1.0
 */
public class MybatisFactory{
   private static final SqlSessionFactory sqlSessionFactory;

   static {
       PooledDataSource pooledDataSource = new PooledDataSource();
       pooledDataSource.setDriver("com.mysql.cj.jdbc.Driver");
       pooledDataSource.setUrl("jdbc:mysql://localhost:3306/test");
       pooledDataSource.setUsername("root");
       pooledDataSource.setPassword("123456");

       TransactionFactory transactionFactory = new JdbcTransactionFactory();

       Environment environment = new Environment("development", transactionFactory, pooledDataSource);

       Configuration configuration = new Configuration(environment);

       // 注意该例中，configuration 添加了一个映射器类（mapper class）。
       // 映射器类是 Java 类，它们包含 SQL 映射语句的注解从而避免依赖 XML 文件。
       // 不过，由于 Java 注解的一些限制以及某些 MyBatis 映射的复杂性，
       // 要使用大多数高级映射（比如：嵌套联合映射），仍然需要使用 XML 配置。
       // 有鉴于此，如果存在一个同名 XML 配置文件，MyBatis 会自动查找并加载它
       // （在这个例子中，基于类路径和 UserMapper.class 的类名，会加载 UserMapper.xml）。
       // 此处必须要把UserMapper.class 和 UserMapper.xml 放在同一个目录下,因为在源码中写死了读同一路径xml，
       configuration.addMapper(UserMapper.class);

       sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
   }

   public static SqlSession getSqlSession(boolean autoCommit) {
       return sqlSessionFactory.openSession(autoCommit);
   }
}
