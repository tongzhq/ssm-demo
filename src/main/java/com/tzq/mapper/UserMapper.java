package com.tzq.mapper;

import com.tzq.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);
    // 使用代码方式构建sqlSessionFactory时 注册的时mapper.class 使用此处的注解执行sql
    // 如果使用mapper.xml执行sql  需要将mapper.xml放在此类一个目录 并且文件名相同  源码中就是如此解析的
//    @Select("select * from user where id = #{id}")
    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
}