package com.tzq.service;

import com.tzq.entity.User;
import org.springframework.transaction.annotation.Transactional;

/**
 * Program Name: mybatis-demo
 * <p>
 * Description:
 * <p>
 * Created by tongzhq on 2021/12/28 16:29
 *
 * @author tongzhq
 * @version 1.0
 */
public interface UserService {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    @Transactional
    int updateByPrimaryKey(User record) throws Exception;
}
