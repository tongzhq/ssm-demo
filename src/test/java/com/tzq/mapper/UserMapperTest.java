package com.tzq.mapper;

import com.tzq.entity.User;
import com.tzq.factory.MybatisFactory;
import com.tzq.utils.MybatisUtil;
import junit.framework.TestCase;
import org.apache.ibatis.datasource.DataSourceFactory;
import org.apache.ibatis.session.SqlSession;

/**
 * Program Name: mybatis-demo
 * <p>
 * Description:
 * <p>
 * Created by tongzhq on 2021/12/28 11:33
 *
 * @author tongzhq
 * @version 1.0
 */
public class UserMapperTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
    }

    public void testDeleteByPrimaryKey() {
    }

    public void testInsert() {
        SqlSession sqlSession = MybatisUtil.getSqlSession(true);
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        int res = mapper.insert(User.builder()
                        .age(33)
                        .name("覆盖广泛的广泛的")
                .build());
        System.out.println(res);
        sqlSession.close();
    }

    public void testSelectByPrimaryKey() {

        SqlSession sqlSession = MybatisFactory.getSqlSession(true);
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User user = mapper.selectByPrimaryKey(1);
        System.out.println(user);

    }

    public void testUpdateByPrimaryKey() {
    }
}