package com.tzq.service;

import com.tzq.config.AppConfig;
import com.tzq.entity.User;
import com.tzq.service.impl.UserServiceImpl;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Program Name: mybatis-demo
 * <p>
 * Description:
 * <p>
 * Created by tongzhq on 2021/12/28 16:37
 *
 * @author tongzhq
 * @version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class UserServiceTest {


    @Resource
    private UserService userService;

    public void testDeleteByPrimaryKey() {
    }

    public void testInsert() {
    }

    public void testInsertSelective() {
    }

    @Test
    public void testSelectByPrimaryKey() {
//        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
//        UserService userService = context.getBean(UserServiceImpl.class);
        User user = userService.selectByPrimaryKey(1);
        System.out.println(user);
    }



    @Test
    public void testUpdateByPrimaryKey() throws Exception {
        int res = userService.updateByPrimaryKey(User.builder()
                        .name("fffffffffffff")
                        .id(1)
                        .age(33)
                .build());
        System.out.println(res);
        System.out.println(userService.selectByPrimaryKey(1));
    }
}